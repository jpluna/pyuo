###
import numpy as np
import pandas as pd
import pyuo.descent as ds
import os
###

def makeTest(problemSet, algSet, maxIter=50, tol=1e-5):
    '''
    problemSet: dictionary whose values are list [bb, bbContext, x0]
    algSet: a dictionary whose are list of [dd, ddContext, ls, lsContext]
    '''

    tab={}
    glogDict = {}
    xlogDict = {}
    for prob in problemSet:
        bb, bbContext, x0 = problemSet[prob]
        xlogDict[prob] = []
        glogDict[prob] = []
        for ss in algSet: 
            dd, ddContext, ls, lsContext = algSet[ss] 
            xlog = 'x--{}--{}.log'.format(prob, ss)
            glog = 'g--{}--{}.log'.format(prob, ss)

            xlogDict[prob].append(xlog)
            glogDict[prob].append(glog)

            result = ds.minimize(x0, bb=bb,  dd=dd, ddContext=ddContext, ls=ls, lsContext=lsContext, xLog=xlog, globalLog=glog, maxIter=maxIter, tol=tol, algorithm=None)
            tab[(prob, ss)] = {'nIter': result.nIter,  'f_last': result.f_last,'gradInfNorm': result.gradInfNorm, 'status': result.status, 'time': result.time}

    tab = pd.DataFrame(tab).T
    tab.nIter = tab.nIter.astype(int)
    tab.status = tab.status.astype(int)

    # showFigures = 'y' in input('Show figures? (y/[n]):').lower()

    # if showFigures:
        # print('making figures...')
        # import pyuo.tools as tt
        # for nn in glogDict: tt.plotLog(glogDict[nn], bokeh=False)
        # for nn in xlogDict: tt.plot(bb=probSet[nn][0], files=xlogDict[nn])
    return tab, xlogDict, glogDict


def makeReport(problemSet, algSet, maxIter=50, tol=1e-5):
    '''
    problemSet: dictionary whose values are list [bb, bbContext, x0]
    algSet: a dictionary whose are list of [dd, ddContext, ls, lsContext]
    '''

    import  weasyprint
    import jinja2
    import pyuo.tools as tt
    import io
    import matplotlib.pyplot as plt
    ###


    template = '''
    <body>
    <h1>Report</h1>
    The maximum number of iterations allowed is {{maxIter}} <br>
    and the tolerance is {{tol}}<br>
    The followin table shows the results:
    {{tab.to_html()}}

    </br>
    The followign figures shows some indications about the performance
    {% for ff in fvalfig %}
    <h2> {{ff}} </h2>
    <img src="file:{{ fvalfig[ff] }}" alt="Chart" height="100" width="200">
    {% if ff in pathfig %}
    <img src="file:{{ pathfig[ff] }}" alt="Chart" height="100" width="200">
    {% endif %}
    <br>
    {% endfor %}

    </body>
    '''
    jTemplate = jinja2.Environment(loader=jinja2.BaseLoader).from_string(template)


    tab, xlogDict, glogDict = makeTest(problemSet, algSet, maxIter=maxIter, tol=tol)
    aux = {}
    xaux={}

    for gg in glogDict: 
        ccname = 'cc-{}.png'.format(gg)
        fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(7, 10))
        fvalfig, gradfig= tt.plotLog(glogDict[gg],axf=ax1, axg=ax2, bokeh=False)
        fig.savefig(ccname)
        aux[gg] =  ccname

        if len(problemSet[gg][-1]) == 2:
            xcname = 'xc-{}.png'.format(gg)
            fig = plt.figure(figsize=(8,8))
            ax = fig.gca()
            tt.plot(bb=problemSet[gg][0], bbContext= problemSet[gg][1], ax=ax,  files= xlogDict[gg])
            fig.savefig(xcname)
            xaux[gg] = xcname

    text = jTemplate.render({'maxIter':maxIter, 'tol':tol,'tab': tab, 'fvalfig': aux, 'pathfig': xaux})

    pdf = weasyprint.HTML(string=text).write_pdf()
    open('./resport.pdf', 'wb').write(pdf)
    ##

