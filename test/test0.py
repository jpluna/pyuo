import pyuo.testF as tf
import numpy as np
# from makeReport import makeReport as mr
from makeReportAlg import makeReport as mr

probSet = { 
        # 'rosenbrock':( tf.rosenbrock, None, 0.5 * np.ones(2)), 
        'quad': (tf.quad, np.array( [[2, 1], [1, 3]]), 0.5 * np.ones(2)), 
        'perturbedQuad': (tf.perturbedQuad, None, 0.5 * np.ones(2)),
        'sugar': (tf.sugar, None, -0.2 * np.ones(2)), 
        }
algSet = {
        'grad-armijo': ('gradient', None, 'armijo', {'messageLevel': 2}),
        # 'grad-armijo': ('gradient', None, 'armijo', None),
        'grad-wolfe': ('gradient', None, 'wolfe',{'messageLevel': 2} ),
        # 'bfgs': ( 'bfgs', None, None, None),
        # 'lm-bfgs-size:2': ('lm-bfgs', {'size':2}, None, None)
        }

algSet = [
        # 'bfgs',
        'bb1',
        'bb2',
        # 'lmbfgs',
        # 'cg',
        ]
tab= mr(probSet, algSet, maxIter=3500, tol=1e-5)
