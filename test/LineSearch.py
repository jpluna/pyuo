import pyuo
import pyuo.testF as tf
import pyuo.tools as tls
import numpy as np

bfgs = True

quadBB = tf.quad

sugarBB = tf.sugar

def bbB3(x, mode=0, context=None):
    x1 = x[0]
    x2 = x[1]
    if mode == 0:
        status = 0
        return x1**4 - x1**3 + x1*x2 + x2**2, status
    elif mode == 1:
        status = 0
        return np.array([4*x1**3 - 3*x1**2 + x2, x1 + 2*x2]), status
    elif mode == 2:
        status = 0
        return x1**4 - x1**3 + x1*x2 + x2**2, np.array([4*x1**3 - 3*x1**2 + x2, x1 + 2*x2]), status

import pyuo.descent as dc




# bb= sugarBB
#bb = quadBB
bb = bbB3
dd= 'gradient'
ls = 'constant'
#ls='cauchy'
ls='armijo'
dd='lm-bfgs'


#lsContext=0.1
#lsContext={'sigmaA':0.9}
lsContext={'messageLevel':4, 'stepSize': 1 }
ddContext={'messageLevel':6, 'memoryMaxSize': 3}


bb=sugarBB
dd='newton'
ls='constant'
lsContext = 1.0
ddContext={'messageLevel':6, 'bb': bb}


if bfgs:
    dd = 'bfgs'
    ddContext = {'messageLevel': 6}
    ls='wolfe'
    lsContext = {'messageLevel':6, 'stepSize': 1}





xlog = '-'.join([dd, ls, 'x2.log'])
glog = '-'.join([dd, ls, 'g2.log'])

x0=np.array([0.1, -0.1])

result = dc.minimize(x0, bb=bb, dd=dd, ddContext=ddContext, ls=ls,lsContext=lsContext, xLog=xlog, globalLog=glog, maxIter=20)
print(result)

# _=tls.plot(bb=bb, files=['gradient-constant-x.log', 'gradient-armijo-x.log', 'gradient-armijo-x1.log', 'gradient-armijo-x2.log'])
_=tls.plot(bb=bb, files=[xlog], save='jj.png')
