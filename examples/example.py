
import numpy as np
import pyuo.descent as dc

def fun(x):
	val = (x[0] - 1) ** 2 + (np.dot(x,x) - 0.25) ** 2
	return val
def grad(x):
    g = (4*np.dot(x,x) - 1)*x
    g[0] = g[0] + 2*(x[0]-1)
    return g


x0 = np.ones(2)
# result = dc.minimize(x0, fun=fun, dd='bfgs', ls='wolfe', globalLog='./jj.log', xLog='x.log')
# result = dc.minimize(x0, fun=fun, dd='gradient', ls='armijo', lsContext={'messageLevel': 4}, step0=0.4, globalLog='./jj.log', xLog='x.log')
algorithm=None
algorithm = 'cg'
algorithm = 'bfgs'
algorithm = 'lmbfgs'
algorithm = 'bb1'
algorithm = 'bb2'
result = dc.minimize(x0, fun=fun, grad=grad, dd='gradient', ls='armijo', lsContext={'messageLevel': 4}, step0Fun='fletcher optimal', step0Context={'optimalValue': 0} , globalLog='./jj.log', xLog='x.log', algorithm=algorithm)
print(result)


# import pyuo.tools
# pyuo.tools.plot(fun=fun, files=[('x.log', 'r')], save='jj.png')
