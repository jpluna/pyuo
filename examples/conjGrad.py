###
import numpy as np
import pyuo
import pyuo.testF
###
bb = pyuo.testF.quad
bbContext = np.array([[1,2],[2,5]])
x0 = np.ones(2)
###
result = pyuo.descent.minimize(x0, bb=bb, bbContext=bbContext, dd='conjugatedGradient',  globalLog='./jj.log', xLog='x.log')
# result = pyuo.descent.minimize(x0, bb=bb, bbContext=bbContext, dd='lm-bfgs', ddContext={'size':2},  globalLog='./jj.log', xLog='x.log')
###
ls = 'constant'
LSContext=1
result = pyuo.descent.minimize(x0, bb=bb, bbContext=bbContext, ls=ls, lsContext=LSContext, dd='grad',  globalLog='./jj.log', xLog='x.log')
###

print(result)
import pyuo.tools
pyuo.tools.plot(bb=bb, files=[('x.log', 'r')], save='jj.png')
###
