'''
Descent Direction  Module
=========================
This module provides some Descent Direction routines
matrixBasedBFGSDirection
minusGrad

Any method follows the same pattern: 
def  <subroutine_name>(x, g, context=None, state=None):
    x: numpy array
        current point
    g: numpy array
        gradient at x
    return d, status
        d: numpy array
            descent direction
        status: int
            0 if success

'''

import numpy as np

def getDD(name):
    if name == 'grad':
        dd = minusGrad
    elif name == 'newton':
        dd = newtonDD
    elif name == 'bb1':
        dd = BB1Direction
    elif name == 'bb2':
        dd = BB2Direction
    elif name == 'bfgs':
        dd = matrixBasedBFGSDirection
    elif name == 'lmbfgs':
        dd = limitedMemoryBFGS
    elif name == 'cg':
        dd = conjugatedGradient
    else:
        print('{} direction not found. Gradient direction will be used')
        dd = minusGrad
    return dd

def minusGrad(x, g, context=None, state=None):
    '''
    x: current point
    g: gradient at current point
    context: some data requiered for the function
    '''
    d = -g
    status = 0
    return d, status

def newtonDD(x, g, context=None, state=None ):
    status = 0 
    messageLevel = 0

    # updating parameters values from context
    if (context is not None):
        if 'messageLevel' in context: messageLevel = context['messageLevel']

    writeMessageLevel = {i: ((state is not None) and (messageLevel >= i)) for i in range(1,6)}

    if context is not None:
        if 'bb' in context:
            bb  = context['bb']
            if 'bbContext' in context:
                bbContext = context['bbContext']
            else:
                bbContext = None

            hess, status = bb(x, mode=3, context=bbContext)
            if status == 0:
                try: 
                    aux =  np.linalg.solve(hess, g)
                    d = -aux
                    status = 0
                except:
                    d = None
                    status = 1 
                    if writeMessageLevel[1]: state.writeGLog(message= 'newton: Linear system failed to be solved')
            else:
                d = None
                if writeMessageLevel[1]: state.writeGLog(message= 'newton: bb failed to computed a hessian. status={}'.format(status) )
        else:
            d = None
            status = 1 
            if writeMessageLevel[1]: state.writeGLog(message= 'newton: no bb in newton context for computing hessian' )

    else:
        d = None
        status = 1 
        if writeMessageLevel[1]: state.writeGLog(message= 'newton: newton context is None' )

    return d, status

def BB1Direction(x, g, context=None, state=None ):
    '''
    dd = - (s*s/s*y)grad
    '''

    messageLevel = 0

    # updating parameters values from context
    if (context is not None):
        if 'messageLevel' in context: messageLevel = context['messageLevel']
    writeMessageLevel = {i: ((state is not None) and (messageLevel >= i)) for i in range(1,6)}


    n = len(x)
    status = 0
    eps = 1e-5

    if 'xp' in context: # xp (previous point available)
        s = x - context['xp']
        y = g - context['gp']
        sy = np.dot(s,y)
        if sy < eps :
            if writeMessageLevel[1]: state.writeGLog(message= 'BB1:small or negative <s,y>={}'.format(sy))
            # error, it is not possible to genrate a descent direction
        sigma = np.dot(s,s)/sy
        context['xp'][:] = np.array(x)
        context['gp'][:] = np.array(g)
    else:
        context['xp'] = np.array(x)
        context['gp'] = np.array(g)
        sigma=0.01

    d = -sigma*g
    return d, status

def BB2Direction(x, g, context=None, state=None ):
    '''
    dd = - (s*y/y*y)grad
    '''

    messageLevel = 0

    # updating parameters values from context
    if (context is not None):
        if 'messageLevel' in context: messageLevel = context['messageLevel']
    writeMessageLevel = {i: ((state is not None) and (messageLevel >= i)) for i in range(1,6)}


    n = len(x)
    status = 0
    eps = 1e-5

    if 'xp' in context: # xp (previous point available)
        s = x - context['xp']
        y = g - context['gp']
        sy = np.dot(s,y)
        if sy < eps :
            if writeMessageLevel[1]: state.writeGLog(message= 'BB2:small or negative <s,y>={}'.format(sy))
            # error, it is not possible to genrate a descent direction
        sigma = sy/np.dot(y,y)
        context['xp'][:] = np.array(x)
        context['gp'][:] = np.array(g)
    else:
        context['xp'] = np.array(x)
        context['gp'] = np.array(g)
        sigma=0.01

    d = -sigma*g
    return d, status



### matrix based bfgs direction
def matrixBasedBFGSDirection(x, g, context=None, state=None ):

    messageLevel = 0

    # updating parameters values from context
    if (context is not None):
        if 'messageLevel' in context: messageLevel = context['messageLevel']
    writeMessageLevel = {i: ((state is not None) and (messageLevel >= i)) for i in range(1,6)}


    n = len(x)
    status = 0
    eps = 1e-5


    if 'W' not in context: context['W'] = np.eye(n, dtype=x.dtype)


    # updating matrix

    if 'xp' in context:
        s = x - context['xp']
        y = g - context['gp']
        sy = np.dot(s,y)
        if sy < eps :
            if writeMessageLevel[1]: state.writeGLog(message= 'bfgs:small or negative <s,y>={}'.format(sy))
            # error, it is not possible to genrate a descent direction
            # if globalLogFile is not None: globalLogFile.write('# BFGS Descent Direction: error, it is not to update BFGS matrix. <s,y>={}\n'.format(sy))
            status = 1
        else: 
            A = np.eye(n, dtype=x.dtype) - (np.dot(np.array([s]).T, np.array([y]))/sy) # this will be used for defined the factor A = I - syT/sy, then W = AWA.T + ssT/sy  Note that np.array([s]) is a row matrix
            context['W'][:] =  A.dot(context['W'].dot(A.T)) + (np.dot(np.array([s]).T, np.array([s]))/sy)
            context['xp'][:] = np.array(x)
            context['gp'][:] = np.array(g)
            status =0
    else:
        context['xp'] = np.array(x)
        context['gp'] = np.array(g)
        status =0

    d = -np.dot(context['W'], g)
    return d, status

class lmBFGSmemory():
    # memnver in memroy index goes from older to newer
    maxSize=None
    size = None
    __s__ = None # the elements are rows
    __y__ = None
    index = None
    def __init__(self, x, maxSize=5):
        self.maxSize = maxSize
        self.__s__ = np.empty( (maxSize, len(x)), dtype=x.dtype)
        self.__y__ = np.empty_like(self.__s__)
        self.__sy__ = np.empty_like(self.__s__[:,0]) ## inner product between  sk and yk
        self.size = 0
        self.index= np.arange(self.maxSize)

def limitedMemoryBFGS(x, g, context=None, state=None):

    messageLevel = 0

    # updating parameters values from context
    if (context is not None):
        if 'messageLevel' in context: messageLevel = context['messageLevel']

    writeMessageLevel = {i: ((state is not None) and (messageLevel >= i)) for i in range(1,6)}


    # the las element of the memory is the pair (x, g). Whenever executing for xp, gp, there tyou be computing in that ple xp-x, gp-g
    status = 0
    memory = context['memory']

    if writeMessageLevel[2]: state.writeGLog(message= 'lm-bfgs: memory size={}'.format(memory.size))

    if memory.size > 0 :
        # updating the last value of xp-x and gp-g
        ind = memory.index[memory.size - 1] # last element in memory
        # print(memory.index)
        # print(memory.__sy__)
        memory.__s__[ind,:]  = x - memory.__s__[ind,:]
        memory.__y__[ind,:]  = g - memory.__y__[ind,:]
        memory.__sy__[ind]  = np.dot(memory.__s__[ind], memory.__y__[ind])

        if writeMessageLevel[4]: state.writeGLog(message= '\tsy={}'.format(memory.__sy__[ind] ))

        # computing new descent direction using information in memory
        q = np.array(g)
        alpha = []
        for ind in memory.index[:memory.size][::-1]: #iterating from newer to older
            s = memory.__s__[ind] 
            y = memory.__y__[ind] 
            sy = memory.__sy__[ind] 
            aa = np.dot(q, s) / sy
            q[:] = q - aa * y
            alpha.append(aa)

        alpha = alpha[::-1] # setting alpha from older to newer
        h = q
        
        for aa, ind in zip(alpha, memory.index[:memory.size]): #iterating from  older to newer
            s = memory.__s__[ind] 
            y = memory.__y__[ind] 
            sy = memory.__sy__[ind] 

            beta = np.dot(h,y) / sy
            h[:] = h + (aa - beta) * s
        d = -h
    else: # memory empty, The descent direction is -grad
        d = -g

    # updating memory
    if memory.size < memory.maxSize: # memory is not full
        ind = memory.index[memory.size] # index for new elements
        memory.size += 1
    else:
        # memory is full
        # older item will be updated
        ind = memory.index[0]
        memory.index[:-1] = memory.index[1:]
        memory.index[-1] = ind

    memory.__s__[ind,:] = x
    memory.__y__[ind,:] = g

    if writeMessageLevel[5]: state.writeGLog(message= 'lm-bfgs: <g,d>={}'.format(np.dot(g,d)))

    return d, status

def conjugatedGradient(x, g, context=None, state=None):
    status = 0
    g_square  = np.dot(g,g)
    if 'd_prev' not in context:
        d = -g
    else:
        ck = - g_square / context['d_prev'][0][1]
        d = -g  + ck * context['d_prev'][0][0]
    context['d_prev']=[(d, g_square)]
    return d, status
