'''
Line Search Module
==================
- This module provides some Line Search routines:
- constLS
- ArmijoLS
- WolfeLS

Any method follows the same pattern: 
    def  <subroutine_name>(xk, fk, gk, dk, bb,  bbContext=None, bbfg=False, t0=None, context=None, state=None):
    return xNext, fNext, gNext, status

Interpolation methods

def <name>(L, R, fL=None,  gL=None,  fR=None, context=None):
    return  t, status

'''
import numpy as np

def getLS(name): 
    name=name.lower()
    if 'constant' in name:
        ls = constLS
    elif 'armijo' in name:
        ls = ArmijoLS
    elif 'wolfe'in name:
        ls = WolfeLS
    elif 'cauchy'in name:
        ls = CauchyLS
    else:
        print('warning: {} line search not found. Armijo LS will be used instaead'.format(name))
        ls = ArmijoLS
    return ls




### Interpolation
def bisectionInter(L, R, fL=None,  gL=None,  fR=None, context=None):
    status = 0
    if context is None:
        factor = 0.5
    else:
        factor = context['factor']
    return  factor * L + (1 - factor) * R, status

##### extrapolation
def powerExtra(L, fL=None,  gL=None, context=None):
    status = 0
    factor = 2
    if (context is not None) and ('factor' in context):
        factor = context['factor']
    return  factor * L, status



### Initial step size
def Fletcher_optimal(x_c, f_c, g_c, d, bb,  bbContext=None, bbfg=False, context=None, state=None):
    '''
    context:{'optimal value': <float>, 'nearOne': <float> }  

    In this case the initial step size should be 
    t0 = (optimal value - f_c)/ <g_c, c>

    It retuns 1 if there is any erro
    '''
    if context is not None:
        if 'optimalValue' in context: 
            optimalValue = context['optimalValue']
            t0 = (optimalValue - f_c) / np.dot(g_c, d)
        if  'nearOne' in context:
            if np.abs(t0-1) <= context['nearOne']: t0 = 1
        if t0 <= 0.5: t0=1
    else:
        t0 = 1
    return t0



### Line Search
def constLS(x_c, f_c, g_c, d, bb,  bbContext=None, bbfg=False,t0=None, context=None, state=None):
    '''
    x_c, f_c, g_c: current point, function value and gradient, respectively
    d: descent direction
    bb: blackbox
    bbContext: blackBox context
    '''
    stepSize = t0
    if context is not None:
        stepSize = float(context)
    xNext = x_c + stepSize * d
    fNext, gNext, statusBB = bb(xNext, mode=2, context=bbContext)
    return xNext, fNext, gNext, statusBB

### Armijo line search

def ArmijoLS(xk, fk, gk, dk, bb,  bbContext=None, bbfg=False,t0=None, context=None, state=None):
    '''
    xk, fk, gk: current point, function value and gradient, respectively
    dk: descent direction
    bb: blackbox
    bbContext: blackBox context
    context: dict
        ArmijoLS context. It may have any/all of  the following fields:
            - interFun: function for made interpolations
            - interContext: context for 'interFun'
            - sigmaA: decrease parameter
            - maxIter: maximum number of iteration
            - stepSize: Initial stepSize
            - messageLevel: int (0,1,2) delfault 0: level of detailed messages sent by LS
            - minStepSize: float, default None: If not None, the first step below is accpeted
        '''
    # setting default parameters values
    interFun = bisectionInter
    interContext = None
    sigmaA = 0.5
    maxIter = 50
    stepSize = t0
    messageLevel = 0
    minStepSize = None

    # updating parameters values from context
    if (context is not None):
        if 'interFun' in context: interFun = context['interFun']
        if 'interContext' in context: interContext = context['interContext']
        if 'sigmaA' in context: sigmaA = context['sigmaA']
        if 'maxIter' in context: maxIter = context['maxIter']
        if 'messageLevel' in context: messageLevel = context['messageLevel']
        if 'stepSize' in context: stepSize = context['stepSize']
        if 'minStepSize' in context: minStepSize = float(context['minStepSize'])

    writeMessageLevel = {i: ((state is not None) and (messageLevel >= i)) for i in range(1,10)}

    stepSize0 = stepSize

    if writeMessageLevel[4]: state.writeGLog(message= 'testing {}->'.format(stepSize))

    iterCount = 0
    sigmaAgkdk = sigmaA * np.dot(gk, dk)
    L = 0

    xNext = xk + stepSize * dk
    if bbfg:
        fNext, gNext, bbStatus = bb(xNext, mode=2, context=bbContext)
    else:
        fNext, bbStatus = bb(xNext, mode=0, context=bbContext)
        gNext = None
    fDes = fk + stepSize * sigmaAgkdk

    if writeMessageLevel[6]: state.writeGLog(message= '{} <= {}?'.format(fNext, fDes) )

    ArmijoOK = (fNext <= fDes)

    if writeMessageLevel[4]: state.writeGLog(message= '\tArmijo ' + ('OK' if ArmijoOK else 'Failed'))
    
    minStepReached = False

    while (not ArmijoOK) and (iterCount < maxIter) and (not minStepReached): 
        iterCount +=1
        R = stepSize
        stepSize, interFunStatus = interFun(L, R, fL=fk, fR=fNext, gL=gk, context=interContext)

        if writeMessageLevel[4]: state.writeGLog(message= 'testing {}->'.format(stepSize))

        xNext = xk + stepSize * dk
        if bbfg:
            fNext, gNext, bbStatus = bb(xNext, mode=2, context=bbContext)
        else:
            fNext, bbStatus = bb(xNext, mode=0, context=bbContext)
            gNext = None
        fDes = fk + stepSize * sigmaAgkdk
        if writeMessageLevel[6]: state.writeGLog(message= '{} <= {}?'.format(fNext, fDes) )
        
        ArmijoOK = (fNext <= fDes)

        if (not ArmijoOK) and ( minStepSize is not None):
            minStepReached = (stepSize <= minStepSize)
            print('minStepSize: {}'.format(minStepReached))
            if minStepReached:
                if writeMessageLevel[1]: state.writeGLog(message= '\t Minimum step size reached without satisfying Armijo condition'  )


        if writeMessageLevel[4]: state.writeGLog(message= '\tArmijo ' + ('OK' if ArmijoOK else 'Failed'))

    ## writing message in gLog
    if writeMessageLevel[1]:
        message = ''

        if writeMessageLevel[2]:
            message = 'stepSize={stepSize}, initial stepSize={stepSize0},  nIter={iterCount}'.format(stepSize0=stepSize0, stepSize=stepSize, iterCount=iterCount)
        else:
            message  = 'stepSize: {}'.format(stepSize) 

        message += (iterCount>= maxIter) * 'maximum number of iteration reached'
        message = 'ArmijoLS: ' +  message
        state.writeGLog(message=message)

    if not bbfg: 
        gNext, bbStatus = bb(xNext, mode=1, context=bbContext)
    return xNext, fNext, gNext, bbStatus

####### Wolfe Linear Search

def WolfeLS(xk, fk, gk, dk, bb,  bbContext=None, bbfg=False,t0=None, context=None, state=None):
    '''
    x_c, f_c, g_c: current point, function value and gradient, respectively
    d: descent direction
    bb: blackbox
    context: dict
        WolfeLS context. It may have any/all of  the following fields: 
            - interFun: function for made interpolations
            - extrapFun: function for made extrapolation
            - interContext: context for 'interFun'
            - extrapContext: context for 'interFun'
            - sigmaA: decrease parameter
            - sigmaC :  Curvature parameter
            - maxIter: maximum number of iteration
            - stepSize: Initial stepSize
            - epsStep: positive value for stopping algorithm if R-L <= epsStep
            - debugMessage: True/False Default: False
            - messageLevel: int (0,1,2) delfault 0: level of detailed messages sent by LS
    '''
    if ((context is not None) and ('debugMessage' in context)):
        debugMessage = context['debugMessage']
    else: 
        debugMessage = False #Just to show information of the line search for debuggging
    ## Setting default parameters
    interFun = bisectionInter
    extrapFun = powerExtra
    interContext = None
    extrapContext = None
    sigmaA = 0.2 # Armijo parameter
    sigmaC = 0.8 # Curvature parameter
    maxIter = 50
    stepSize = t0
    epsStep = 1e-5
    messageLevel = 0

    # updating parameters values from context
    if (context is not None):
        if 'interFun' in context: interFun = context['interFun']
        if 'extrapFun' in context: extrapFun = context['extrapFun']
        if 'interContext' in context: interContext = context['interContext']
        if 'extrapContext' in context: extrapContext = context['extrapContext']
        if 'sigmaA' in context: sigmaA = context['sigmaA']
        if 'sigmaC' in context: sigmaC = context['sigmaC']
        if 'maxIter' in context: maxIter = context['maxIter']
        if 'epsStep' in context: epsStep = context['epsStep']
        if 'messageLevel' in context: messageLevel = context['messageLevel']

    writeMessageLevel = {i: ((state is not None) and (messageLevel >= i)) for i in range(1,6)}

    stepSize0 = stepSize
    iterCount = 0

    t = stepSize # initial step

    wolfeOK = False
    L = 0
    fL = fk
    gL = np.array(gk)
    R = -1
    sigmaAdg = sigmaA * np.dot(dk, gk)
    sigmaCdg = sigmaC * sigmaAdg / sigmaA
    x = np.empty_like(xk)
    gx = np.empty_like(xk)
    bestArmijoStep= 0
    windowSizeOK = True




    while ((not wolfeOK) and (iterCount < maxIter) and windowSizeOK):
        iterCount += 1
        if debugMessage: print('[k={}],[t={}][{} -- {}]'.format(iterCount,t,L,R))
        x[:] = xk + t * dk #waring with memory management

        if bbfg:
            fx, gx[:], status = bb(x, mode=2, context=bbContext)
        else:
            fx, status = bb(x, mode=0, context=bbContext)
        # Checking Armijo condition
        armigo = (fx <= fk + t * sigmaAdg)
        if debugMessage: print('sigmaAdg={}'.format(sigmaAdg))
        if debugMessage: print('[k={}],[t={}][fk={} -- fx={}]'.format(iterCount,t,fk,fx))
        if debugMessage: print('[k={}],[t={}]Armijo OK'.format(iterCount,t))

        if writeMessageLevel[4]: state.writeGLog(message= 'testing t={}->'.format(t))
        if armigo:
            if debugMessage: print('[k={}],[t={}]Armijo OK'.format(iterCount,t))
            if writeMessageLevel[4]: state.writeGLog(message= '\tArmijo OK'.format(t))
            # Armijo OK
            # Checking curvature condition 
            if t>bestArmijoStep: bestArmijoStep = t 

            if not bbfg: #gradient need to be computed
                gx[:], status = bb(x, mode=1, context=bbContext) # be careful with memory management

            curvatureCondition = (sigmaCdg <= np.dot(gx, dk))
            if curvatureCondition:
                if writeMessageLevel[4]: state.writeGLog(message= '\tCurvature OK'.format(t))
                wolfeOK = True
                if debugMessage: print('[k={}],[t={}] Wolfe OK'.format(iterCount, t))
                # return x, fx, gx, status
            else:
                if writeMessageLevel[4]: state.writeGLog(message= '\tCurvature Failed'.format(t))
                if debugMessage: print('[k={}],[t={}] Not curvatureCondition'.format(iterCount, t))
                # Armijo OK, Curvature not
                # to amall step
                L = t
                fL = fx
                gL = gx
                if R < 0:
                    # there are not yet right (large) steps
                    # extrapolation is required
                    if debugMessage: print('extrapolation')
                    t, status = extrapFun(L, fL=fL,  gL=gL, context=extrapContext)
                else:
                    # there is already righ bound R
                    # we interpolate
                    print('intrapolation')
                    t, status = interFun(L, R, fL=fL, gL=gL, fR=fR, context=interContext)                   
        else:
            if writeMessageLevel[4]: state.writeGLog(message= '\tArmijo Failed'.format(t))
            if debugMessage: print('[k={}],[t={}] Not armijo'.format(iterCount, t))
            #  Armijo condition is not satisfied
            # too large step
            R = t
            fR = fx
            # we interpolate
            t, status = interFun(L, R, fL=fL, gL=gL, fR=fR,  context=interContext)                   
            if debugMessage: print('intrapolation') 

        windowSizeOK = ((R < 0) or ( R-L>= epsStep))


    if ((iterCount == maxIter) or (not windowSizeOK)):
        if bestArmijoStep > 0:
            x[:] = xk + bestArmijoStep * dk
            fx, gx[:], status = bb(x, mode=2, context=bbContext)

        status = 1

    ## writing message in gLog
    if writeMessageLevel[1]:
        message = ''
        if writeMessageLevel[2]:
            message = 'stepSize={stepSize}, initial stepSize={stepSize0},  nIter={iterCount}'.format(stepSize0=stepSize0, stepSize=t, iterCount=iterCount)
        else:
            message  = 'stepSize: {}'.format(t) 

        message += (iterCount>= maxIter) * 'maximum number of iteration reached' + (not windowSizeOK) * (', searching windown too small:  {}'.format(R-L))
        if (((iterCount == maxIter) or (not windowSizeOK)) and (bestArmijoStep>0)):
            message += ', using an Armijo step: {} '.format(bestArmijoStep)

        message = 'Wolfe LS: ' + message
        state.writeGLog(message=message)
    return x, fx, gx, status


### Cauchy exact search
def CauchyLS(x_c, f_c, g_c, d, bb,  bbContext=None, bbfg=False,t0=None, context=None, state=None):
    status = 0
    if 'g0' not in context:
        g0, status = bb(0*x_c, mode=1, context=bbContext)
        context['g0'] = g0
    else:
        g0 = context['g0']

    gd, status = bb(d, mode=1, context=bbContext)
    tk  = - np.dot(g_c, d) / np.dot(gd-g0,d)
    
    x = x_c + tk * d
    fx, gx, status = bb(x, mode=2, context=bbContext)
    return x, fx, gx, status
