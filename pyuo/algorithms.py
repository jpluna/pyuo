import numpy as np
import pyuo.DD as DD
import pyuo.LS as LS
import pyuo.common as common
'''
This module configures all components necessary for implenting the algorithm
'''

def __buildAlgContext__(algContext=None):
    algConf = {}
    if algContext is None: # we will use default values
        algContext={}
    for kk in ['dd', 'ddContext', 'ls', 'lsContext']:
        if kk in algContext:
            algConf[kk] = algContext[kk]
        else:
            algConf[kk] = None
    return algContext, algConf

def genericAlgorithm(dd=None, ddContext=None, ls=None, lsContext=None):
    _, algConf = __buildAlgContext__(None)

    if isinstance(dd, str):
        if 'gradient' in dd.lower():
            dd = DD.minusGrad 
        elif 'bfgs' == dd.lower(): 
            dd = DD.matrixBasedBFGSDirection
            if ddContext is None:
                ddContext = {}
            if ls is None:
                print('Usning BFGS. Wolfe line search will be used\n')
                ls = LS.WolfeLS
                lsContext = {'stepSize': 1.0}

        elif 'lm-bfgs' == dd.lower():
            dd = DD.limitedMemoryBFGS
            if ddContext is None:
                ddContext = {}
            # creating memory
            if 'memory' not in ddContext:
                if 'memoryMaxSize' in ddContext:
                    ddContext['memory'] =  DD.lmBFGSmemory(x0, maxSize = ddContext['memoryMaxSize'])
                else:
                    ddContext['memory'] =  DD.lmBFGSmemory(x0)

            print(ddContext)
            if ls is None:
                print('Usning LM-BFGS. Wolfe line search will be used\n')
                ls = LS.WolfeLS
                lsContext = {'stepSize': 1.0}

            dd = DD.conjugatedGradient
            print('Usning Conjugted Gradient. Cauchy Exact line search will be used\n')
            ls = LS.CauchyLS
            if ddContext is None:
                ddContext = {}
            if lsContext is None:
                lsContext = {}
        elif 'newton' == dd.lower(): 
            dd = DD.newtonDD
            if (ddContext is None) or ('bb' not in ddContext):
                print('warning: bb should be included in ddContext')


        # default values for line search
        if isinstance(ls, str):
            if 'constant' in ls:
                ls = LS.constLS
            elif 'armijo' in ls:
                ls = LS.ArmijoLS
            elif 'wolfe'in ls:
                ls = LS.WolfeLS
            elif 'cauchy'in ls:
                ls = LS.CauchyLS
                if lsContext is None:
                    lsContext = {}

    return dict(dd=dd, ddContext=ddContext, ls=ls, lsContext=lsContext)

# smooth unconstrained algorithms

def bfgs(algContext=None):
    algContext, algConf = __buildAlgContext__(algContext)
    if algConf['dd'] is None:  algConf['dd'] = DD.matrixBasedBFGSDirection
    if algConf['ddContext'] is None:  algConf['ddContext'] = {}
    if algConf['ls'] is None:  
        print('Using BFGS. Wolfe line search will be used\n')
        algConf['ls'] = LS.WolfeLS

    if algConf['lsContext'] is None:  algConf['lsContext'] = {'stepSize': 1.0}

    return algConf

def lmbfgs(algContext=None):
    '''
    algContext must have the key '_dim' with the dimension of the problem
    '''

    algContext, algConf = __buildAlgContext__(algContext)
    if algConf['dd'] is None:  algConf['dd'] = DD.limitedMemoryBFGS
    if algConf['ddContext'] is None:  algConf['ddContext'] = {}

    # creating memory for lm-bfgs
    ddContext = algConf['ddContext']
    if 'memory' not in ddContext:
        x0 = algContext['x0']
        if 'memoryMaxSize' in ddContext:
            ddContext['memory'] =  DD.lmBFGSmemory(x0, maxSize = ddContext['memoryMaxSize'])
        else:
            ddContext['memory'] =  DD.lmBFGSmemory(x0)

    print(ddContext)
    
    if algConf['ls'] is None:  
        print('Usning LM-BFGS. Wolfe line search will be used\n')
        algConf['ls'] = LS.WolfeLS

    if algConf['lsContext'] is None:  algConf['lsContext'] = {'stepSize': 1.0}

    return algConf

def cg(algContext=None):
    algContext, algConf = __buildAlgContext__(algContext)
    if algConf['dd'] is None:  algConf['dd'] = DD.conjugatedGradient
    if algConf['ddContext'] is None:  algConf['ddContext'] = {}
    if algConf['ls'] is None:  
        print('Usning Conjugted Gradient. Cauchy Exact line search will be used\n')
        algConf['ls'] = LS.CauchyLS
    if algConf['lsContext'] is None:  algConf['lsContext'] = {}

    return algConf

def bb1(algContext=None):
    algContext, algConf = __buildAlgContext__(algContext)
    if algConf['dd'] is None:  algConf['dd'] = DD.getDD('bb1')
    if algConf['ddContext'] is None:  algConf['ddContext'] = {}
    if algConf['ls'] is None:  
        print('BB1 algorithm: Constant step =1')
        algConf['ls'] = LS.getLS('constant')
    if algConf['lsContext'] is None:  algConf['lsContext'] = 1.0

    return algConf

def bb2(algContext=None):
    algContext, algConf = __buildAlgContext__(algContext)
    if algConf['dd'] is None:  algConf['dd'] = DD.getDD('bb2')
    if algConf['ddContext'] is None:  algConf['ddContext'] = {}
    if algConf['ls'] is None:  
        print('BB2 algorithm: Constant step =1')
        algConf['ls'] = LS.getLS('constant')
    if algConf['lsContext'] is None:  algConf['lsContext'] = 1.0

    return algConf
