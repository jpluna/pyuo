import numpy as np
import pyuo.DD as DD
import pyuo.LS as LS
import pyuo.algorithms as algs
import pyuo.common as common
import os

class algorithmState(): 
    f0=None 
    x0=None
    g0=None
    gNorm0=None
    fLast=None
    xLast=None
    gLast=None
    gNormLast=None
    nIter=None
    context=None
    initCallBackFun=None 
    callBackFun=None 
    endCallBackFun=None 
    xLogFilePath=None
    globalLogFilePath=None
    

    def __init__(self, context=None, initCallBackFun=None, callBackFun=None, endCallBackFun=None, xLogFilePath=None, globalLogFilePath=None): 
        self.context = context
        self.initCallBackFun = initCallBackFun
        self.callBackFun = callBackFun
        self.endCallBackFun = endCallBackFun


        self.xLogFilePath=xLogFilePath
        self.globalLogFilePath=globalLogFilePath

        # erasing old files
        for ff in [xLogFilePath, globalLogFilePath]:
            if ff is not None:
                if os.path.isfile(ff): 
                    os.remove(ff)


    def initCallBack(self): 
        if self.initCallBackFun is not None: 
            self.initCallBackFun(self) 

    def callBack(self): 
        if self.callBackFun is not None: 
            self.callBackFun(self) 

    def endCallBack(self): 
        if self.endCallBackFun is not None: 
            self.endCallBackFun(self) 

    def __repr__(self): 
        return  '\n'.join(['{} = {}'.format(key, val) for key, val in self.__dict__.items() if isinstance(val, (int, float))])

    def writeXLog(self):
        if self.xLogFilePath is not None:
            ff = open(self.xLogFilePath, 'a')
            ff.write('[k={}],'.format(self.nIter) + ','.join([str(xi) for xi in self.xLast]) + '\n')
            ff.close()

    def writeGLog(self, message=None, sharp=True, includeIter=True):
        if self.globalLogFilePath is not None:
            ff = open(self.globalLogFilePath, 'a')
            if message is None: 
                ff.write('[k={}], fval: {}, grad inf-norm: {}\n'.format(self.nIter, self.fLast, self.gNormLast))
            else:
                if sharp:
                    mAux = message.replace('\n', '\n#')
                else:
                    mAux = message
                ff.write( sharp * '#' + includeIter * ' [k={}], '.format(self.nIter) +  '{}\n'.format(mAux))
            ff.close()

def absStoppingCriterion(state, tol=1e-5):
    return state.gNormLast <= tol

def relStoppingCriterion(state, tol=1e-5):
    return (state.gNormLast/(state.gNorm0 + 1)) <= tol


def iterAlg(
        x0, f0=None, grad0=None, # initial 
        bb=None, bbContext=None, 
        bbfg=False,
        dd=None, ddContext=None, 
        ls=None, lsContext=None,
        step0=None,
        step0Fun=None, step0Context=None,
        stopFun=None,
        state=None,
        maxIter=100, 
        tol=1e-4,
        message=None,
        ): 

    import time
    startingTime = time.time()


    if stopFun is None:
        stopFun = absStoppingCriterion
        
    if message is not None: 
        state.writeGLog(message=message, includeIter=False)

    state.x0 = x0

    if ((f0 is None) and (grad0 is None)): 
        fval, g, status = bb(x0, mode=2, context=bbContext)
    else:
        if f0 is None: 
            fval,  status = bb(x0, mode=0, context=bbContext)
        else:
            fval = f0
            status = 0

        if grad0 is None: 
            g,  status = bb(x0, mode=1, context=bbContext)
        else:
            g = grad0
            status = 0



    state.f0 = fval
    state.g0 = g
    state.gNorm0 = max(abs(state.g0)) 

    if status != 0: 
        print('error: BlackBox returned status {}\n'.format(status))
        return None, None, None, status

    state.nIter = 0
    x = np.array(x0)
    g = np.array(state.g0)
    state.fLast = fval
    state.xLast = x
    state.gLast = g
    state.gNormLast = max(abs(state.gLast)) 
    gInfNorm = state.gNormLast

    # if xLog is not None: xLogFile.write( ','.join(['[k={}]'.format(state.nIter)] + [str(val) for val in x]) + '\n') 
    state.writeXLog()

    # if globalLog is not None: globalLogFile.write('[k={}], fval: {}, grad inf-norm: {}\n'.format(state.nIter, fval, gInfNorm))
    state.writeGLog()

    stop = stopFun(state, tol)


    state.initCallBack()
    while ( (not stop) and (state.nIter < maxIter)): 
        state.nIter +=1
        d, status = dd(x, g, context=ddContext, state=state) # , globalLogFile=globalLogFile)
        # d, status = dd(x, g, ddContext, globalLogFile=globalLogFile)

        #setting initial step size
        if step0 is None:
            t0 = step0Fun(x, fval, g, d, bb,  bbContext=bbContext, bbfg=bbfg, context=step0Context, state=state)
        else:
            t0 = step0

        x[:], fval, g[:], status = ls(x, fval, g, d, bb,  bbContext=bbContext,bbfg=bbfg, t0=t0, context=lsContext,state=state) #, globalLogFile=globalLogFile)
        state.fLast = fval

        if status != 0: 
            print('error: Line Search returned status {}\n'.format(status))
            totalTime = time.time() - startingTime
            return common.result_class(status=status, gradInfNorm=state.gNormLast, x_last=x, f_last=fval, g_last=g, nIter=state.nIter, time=totalTime,state=state)

        gInfNorm = max(abs(g)) 
        state.gNormLast = gInfNorm
    

        # if xLog is not None: xLogFile.write( ','.join(['[k={}]'.format(state.nIter)] + [str(val) for val in x]) + '\n')
        state.writeXLog()

        # if globalLog is not None: globalLogFile.write('[k={}], fval: {}, grad inf-norm: {}\n'.format(state.nIter, fval, gInfNorm))
        state.writeGLog()

        state.callBack()
        stop = stopFun(state, tol)

    state.endCallBack()
    totalTime = time.time() - startingTime

    # if globalLog is not None: 
    if ((state.nIter >= maxIter) and (not stop)): 
        status = 100
        state.writeGLog(message='Maximum number  of iteration reached without satisfying stopping criterion', includeIter=False) 
    state.writeGLog(message= '# Gradient Inf-Norm: {}\n# Function Value: {}\n# Time: {}'.format(state.gNormLast, fval, totalTime), includeIter=False, sharp=False)

    
    return common.result_class(status=status, gradInfNorm=state.gNormLast, x_last=x, f_last=fval, g_last=g, nIter=state.nIter, time=totalTime,state=state)

def minimize(x0, f0=None, grad0=None,
        fun=None, grad=None, 
        fg=None,
        bb=None, bbContext=None,
        maxIter=100, tol=1e-4,
        xLog=None, globalLog=None, 
        message=None,
        step0=1.0,
        step0Fun=None, step0Context=None,
        stopFun=None,
        state=None,
        stateContext=None,
        initCallBackFun=None,
        callBackFun=None,
        endCallBackFun=None,
        algorithm='bfgs', algContext=None,
        dd=None, ddContext=None, 
        ls=None, lsContext=None
        ): 
    '''
        x0: np.array: initial point for the algorithm. It is required.
        f0:float, delfault None, function value at x0
        grad0: np.array, default None,gradient value at x0

        fun: function(x:np.array)->fval:float
        grad:function(x:np.array)-> gradient:np.array, 
        fg:function(x:np.array)->fval:float, gradient:np.array,
        bb:function(x:np.array, mode:int, context:dict)->fval:float, gradient:np.array,status:int
        at least one of fun, fg, or bb should be provided. Priority is bb>fg>fun

        algithm:str, name of the algorithm to be used. Default 'bfgs'. If None, the an algorithm will be chosen according to values of dd and ls.
        algContext:dict: paramenters for tuning the algorithm. If None, default values will be chosen.


    '''

    import pyuo.tools

    # Initializing the blackbox

    bbfg=False
    if bb is None:
        if fg is not None: 
            bb = lambda x,mode=0, context=None: pyuo.tools.fgBB(x, mode=mode, context=context, fg=fg)
            bbfg=True
        else:
            if fun is None:
                print('Error: Objective function is required')
                return None
            elif grad is None: 
                    bb = lambda x,mode=0, context=None: pyuo.tools.numBB(x, mode=mode, context=context, fun=fun)
            else: 
                bb = lambda x,mode=0, context=None: pyuo.tools.fgBB(x, mode=mode, context=context, fun=fun, grad=grad)
    

    # default values for initial stepize function
    if step0Fun is not None:
        step0=None
        if isinstance(step0Fun, str):
            if 'fletcher' in step0Fun.lower():
                if step0Context is None:
                    print('step0Context is required.')
                    return None
                else:
                    if 'optimal' in step0Fun.lower():
                        step0Fun = LS.Fletcher_optimal
                        if 'optimalValue' not in step0Context:
                            print('optimalValue is required in step0Context')
                            return None
            else:
                print('Error: initial step size function {} not found.'.format(step0Fun)) 
                return None
    # defining the algorithm
    if algorithm is None: # unamed algorithm. We follow dd and ls variables
        algConf = algs.genericAlgorithm(dd=dd,ddContext=ddContext,ls=ls,lsContext=lsContext)
        # default values for line search
    else: # follow a named algorithm
        if isinstance(algorithm, str):
            if 'bfgs' == algorithm.lower(): 
                algConf = algs.bfgs(algContext)
            elif any([ kk in algorithm.lower() for kk in ['lmbfgs', 'lm_bgfs',  'lm-bgfs']]): 
                ### alg context must have initial point
                if algContext is None:
                    algContext = {}
                if  'x0' not in algContext:
                    algContext['x0'] = x0
                algConf = algs.lmbfgs(algContext)
            elif 'bb1' == algorithm.lower(): 
                algConf = algs.bb1(algContext)
            elif 'bb2' == algorithm.lower(): 
                algConf = algs.bb2(algContext)
            elif ('conjugatedGradient' in algorithm.lower()) or (algorithm.lower()== 'cg'): 
                algConf = algs.cg(algContext)
            else:
                print('{} algorithm is not available')
                return None
        else:
            print('{} algorithm is not available')
            return None


    # default stopping criterion
    if isinstance(stopFun, str):
        if 'absolute' in stopFun.lower():
            stopFun = absStoppingCriterion
        elif 'relative' in stopFun.lower():
            stopFun = relStoppingCriterion
        else:
            print('{} stopping criterion is not availables. Using default stopping criterion\n'.format(stopFun))
            stopFun = None

    if state is None: 
        state = algorithmState(context=stateContext,initCallBackFun = initCallBackFun,callBackFun=callBackFun, endCallBackFun=endCallBackFun, xLogFilePath=xLog, globalLogFilePath=globalLog)

    result = descentM(x0,f0=f0, grad0=grad0,
            bb=bb, bbfg=bbfg, bbContext=bbContext, 
            # dd=dd, ddContext=ddContext, 
            # ls=ls, lsContext=lsContext,
            **algConf,
            step0=step0,
            step0Fun=step0Fun, step0Context=step0Context,
            state=state, maxIter=maxIter, tol=tol, message=message)

    return result
