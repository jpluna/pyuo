'''
The unconstrained (smooth) optimization module. It has the following submodules:
- testF: module of basic test functions
- LS: Line Search module routines
- DD: Descent direction routines
- descent: minimization routines
'''

__all__=['testF', 'descent', 'LS', 'DD']

from . import *
