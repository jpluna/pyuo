# PyUO
This is a python library providing tools for unsconstrained optimization. We have the following submodules 
- ## [tools](./doc/tools.md) 
- ## [Descent Methods](./doc/descent.md) 
- ## [Descent Directions](./doc/DD.md) 
- ## [Line Search](./doc/LS.md) 

Install
=======
via pip from gitlab:

```python
pip install git+https://gitlab.com/jpluna/pyuo.git
```
